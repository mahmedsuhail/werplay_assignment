import homePage from '../support/page_objects/HomePagePO'

const HomePage          = new homePage();
const randomNumber = Math.floor(Math.random() * 1000000);
const email = "automation" + randomNumber + "@mailinator.com"

describe('Test Suite for Case 2', function () {

    before(function () { 
      cy.log("Open the URL");
      cy.visit(Cypress.env('adminUrl'));   
      cy.get(HomePage.getAccountsBtn()).trigger('mouseover')
      cy.get(HomePage.getLogoutBtn()).should('be.visible').click();
      cy.wait(5000); 
  })

  beforeEach(function () {

    cy.fixture(Cypress.env('fixtureFile')).then((data) => {
      this.data = data
    })
    Cypress.Cookies.defaults({
      preserve: (cookie) => {
        return true;
      }
    })
  })

  it('Should authenticate user through facebook', function () {
    cy.get(HomePage.getNoThanksBtn()).click();
    cy.get(HomePage.getNoThanksBtn()).should('not.exist')

    cy.get(HomePage.getAccountBtn()).trigger('mouseover') 
    cy.get(HomePage.getLoginBtn()).click();
    cy.get(HomePage.getFacebookLoginButton()).click();
   
    cy.url().should('eq', this.data.url)      // Verify that user is logged in after authentication
    cy.wait(3000)
    cy.get(HomePage.getClothingButton()).click();
  })

  it('Should verify filters functionality on Clothing Screen', function () {
    cy.clickCheckboxFromFilters(this.data.brandName)         // Select brand Alexia Maria from Designer
    cy.get('.PLP-productList--3 .Product').should('have.length', '4')     // Verify Filter results
    cy.verifyBrandName(this.data.brandName)        // Verify all results have Alexia Maria brand name
  })
 

})
