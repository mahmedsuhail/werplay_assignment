import homePage from '../support/page_objects/HomePagePO'
import signUpPage from '../support/page_objects/SignUpPagePO'
import accountDetailsPage from '../support/page_objects/AccountDetailsPagePO'
import shippingInfoPage from '../support/page_objects/ShippingInfoPagePO'


const HomePage          = new homePage();
const SignUpPage        = new signUpPage();
const AccountDetailsPage        = new accountDetailsPage();
const ShippingInfoPage        = new shippingInfoPage();
const randomNumber = Math.floor(Math.random() * 1000000);
const email = "automation" + randomNumber + "@mailinator.com"

describe('Test Suite for Case 1', function () {

    before(function () { 
      // cy.clearCookie(cookie)
      cy.log("Open the URL");
      cy.visit(Cypress.env('adminUrl'));    
  })

  beforeEach(function () {

    cy.fixture(Cypress.env('fixtureFile')).then((data) => {
      this.data = data
    })
    Cypress.Cookies.defaults({
      preserve: (cookie) => {
        return true;
      }
    })
  })

  it('Should Register a New User', function () {
    cy.get(HomePage.getNoThanksBtn()).click();
    cy.get(HomePage.getNoThanksBtn()).should('not.exist')

    cy.get(HomePage.getAccountBtn()).trigger('mouseover') 
    cy.get(HomePage.getSignUpBtn()).click();
    cy.get(SignUpPage.getFirstName()).type(this.data.firstName)
    cy.get(SignUpPage.getLastName()).type(this.data.lastName)
    cy.get(SignUpPage.getEmail()).type(email)
    cy.get(SignUpPage.getPassword()).type(this.data.password)
    cy.get(SignUpPage.getSignUpBtn()).click();

  })
  it('Should Verify that after logging into the account the “Email-Address” field is un-editable', function () {
   
    cy.get(HomePage.getDeliveryLabel()).eq(0).should('be.visible')
    cy.closeNotificationIfExist()
    cy.get(HomePage.getAccountsBtn()).trigger('mouseover')
    cy.wait(5000);
    cy.get(HomePage.getLogoutBtn()).click();
    cy.wait(5000);
    cy.get(HomePage.getAccountsBtn()).trigger('mouseover')
    cy.get(HomePage.getLoginBtn()).click();
    cy.get(HomePage.getFormEmail()).type(email) //automation116259@mailinator.com
    cy.get(HomePage.getFormPassword()).type(this.data.password) //this.data.password
    cy.get(HomePage.getSignInBtn()).click();
    cy.wait(5000);

    cy.get(HomePage.getAccountsBtn()).trigger('mouseover')
    cy.get(HomePage.getAccountOption()).click();
    cy.get(AccountDetailsPage.getEditBtn()).should('be.visible').click()
    cy.get(AccountDetailsPage.getProfileEmailField()).should('be.disabled')             // Verify Email field is disabled
    cy.get(AccountDetailsPage.getProfileEmailField()).should('contain.value', email)    // Verify email is same 
    cy.get(AccountDetailsPage.getPageHeadingLogo()).click()
  })

  it('Should Update the phone number to “+97167324238” and verify it is updated', function () {
    // cy.get('.Profile-updateDetailsButton').click();
    // Phone Number field is not showing on UI in EDIT Mode
  })

  it('Should add items to Bag', function () {
    // cy.get(HomePage.getBanners()).eq(6).click();
    cy.get(HomePage.getSearchField()).should('be.visible').type(this.data.searchText)
    cy.wait(5000);
    cy.get(HomePage.getSearchResults()).eq(1).should('contain.text', this.data.searchResultText).click()
    
   
    cy.wait(5000);
    cy.clickOnItem('.PLP-productList--3', this.data.itemName, this.data.itemPrice)
    cy.closeNotificationIfExist()
    cy.get(HomePage.getSizeDropdown()).click();
    cy.get(HomePage.getDropdownOptions()).eq(0).click();
    cy.get(HomePage.getAddToBagButton()).click();
    cy.wait(5000);
    cy.closeNotificationIfExist()
    cy.get(HomePage.getSizeDropdown()).click();
    cy.get(HomePage.getDropdownOptions()).eq(1).click();
    cy.get(HomePage.getAddToBagButton()).click();
    cy.wait(5000);
    cy.closeNotificationIfExist()

    cy.get(HomePage.getCartItemName()).eq(0).should('have.text', this.data.itemName)      // Verify Name of Item added in Bag
    cy.get(HomePage.getCartItemPrice()).eq(0).should('have.text', this.data.itemPrice)    // Verify Price of Item added in Bag
    cy.get('.MiniCart-container .CartTotal-viewBag').click();
    cy.get('.CartTotal-secureCheckout').should('be.visible').click();

  })

  it('Should fill in shipping information', function () {
    
    // cy.get(ShippingInfoPage.getFirstNameField()).should('be.visible').type(this.data.firstName)  // Required for the first time only
    // cy.get(ShippingInfoPage.getLastNameField()).type(this.data.lastName)
    // cy.get(ShippingInfoPage.getEmailField()).type(email)
    cy.get(ShippingInfoPage.getNumerField()).type('56455545')
    cy.get(ShippingInfoPage.getRegionField()).click()
    cy.get('.Select-control div[id^="react-select-"]').eq(0).type(this.data.state).type('{enter}')
    cy.get(ShippingInfoPage.getCityField()).type(this.data.city)
    cy.get(ShippingInfoPage.getStreetField()).focus().type(this.data.address)
    cy.wait(3000);
    cy.get(ShippingInfoPage.getSubmitButton()).click()

  })

})
