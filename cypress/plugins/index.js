/// <reference types="cypress" />
const xlsx = require("node-xlsx").default;
const fs = require("fs");
const path = require("path");
// ***********************************************************
// This example plugins/index.js can be used to load plugins
//
// You can change the location of this file or turn off loading
// the plugins file with the 'pluginsFile' configuration option.
//
// You can read more here:
// https://on.cypress.io/plugins-guide
// ***********************************************************

// This function is called when a project is opened or re-opened (e.g. due to
// the project's config changing)

/**
 * @type {Cypress.PluginConfig}
 */

module.exports = (on, config) => {
  on('before:browser:launch', (browser, options) => {
    const downloadDirectory = path.join(__dirname, '..', 'excelDownloads')

    // console.log(options.args)


    if (browser.family === 'chromium' && browser.name !== 'electron') {
      options.preferences.default['download'] = { default_directory: downloadDirectory }
    
    // if (browser.family === 'chromium' && browser.name !== 'electron') {
    //   // auto open devtools
    //   options.args.push('--auto-open-devtools-for-tabs')
    // }

    if (browser.family === 'firefox') {
      // auto open devtools
      options.args.push('-devtools')
    }

    if (browser.name === 'electron') {
      // auto open devtools
      options.preferences.devTools = true
    }

      return options
    }
  })

  on("task", {
        parseXlsx({ filePath }) {
          return new Promise((resolve, reject) => {
            try {
              const jsonData = xlsx.parse(fs.readFileSync(filePath));
              resolve(jsonData);
            } catch (e) {
              reject(e);
            }
          });
        }
      });
}
