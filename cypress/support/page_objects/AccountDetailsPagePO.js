

class AccountDetailsPage {

  getEditBtn()
  {
    return '.MyAccountPage a[href="/customer/information"]';
  }

  getProfileEmailField()
  {
    return '.Profile-email';
  }

  getPageHeadingLogo()
  {
    return '.ProductNavigation-logo';
  }
  
}
export default AccountDetailsPage
