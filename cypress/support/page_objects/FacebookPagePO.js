

class FacebookPage {

    getEmailField()
    {
      return '#email';
    }
  
    getPasswordField()
    {
      return '#pass';
    }

    getLoginButton()
    {
      return '#loginbutton';
    }

   getContinueButton()
    {
      return 'button[name="__CONFIRM__"]';
    }
    
  }
  export default FacebookPage
  