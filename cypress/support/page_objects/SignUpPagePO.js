class SignUpPage {

  getFirstName() 
  {
    return '.Profile-firstName';
  }

  getLastName() 
  {
    return '.Profile-lastName';
  }
  getEmail() 
  {
    return '.Profile-email';
  }
  getPassword() 
  {
    return '.PasswordField-password';
  }
  getSignUpBtn() 
  {
    return '.Profile-signUpButton';
  }
}
export default SignUpPage
