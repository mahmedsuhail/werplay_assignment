

class ShippingInfoPage {

    getFirstNameField()
    {
      return '#firstName';
    }
  
    getLastNameField()
    {
      return '#lastName';
    }

    getEmailField()
    {
      return '#email';
    }
    getNumerField()
    {
      return '#mobileNumber';
    }
    getRegionField()
    {
      return 'label[for="regionId"]';
    }

    getCityField()
    {
      return 'label[for="city"]';
    }
    getStreetField()
    {
      return '#street';
    }
    getSubmitButton()
    {
      return '.ShippingInformationForm-submitButton';
    }
    
  }
  export default ShippingInfoPage
  