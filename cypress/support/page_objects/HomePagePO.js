class HomePage {

  getNoThanksBtn() 
  {
    return '#wzrk-cancel';
  }
  
  getAccountBtn()
  {
    return '.ProductNavigation button';
  }

  getSignUpBtn()
  {
    return '.CustomerPopup-signUpButton';
  }

  getDeliveryLabel()
  {
    return '.SellingPoints-container';
  }

  getBanners()
  {
    return '.Banner';
  }
  getAccountsBtn()
  {
    return '.CustomerPopup .Popup-iconText';
  }
  getLogoutBtn()
  {
    return '.CustomerPopup-loggedIn button';
  }
  getLoginBtn()
  {
    return '.CustomerPopup-signInButton';
  }
  getFormEmail()
  {
    return '.SignInForm-email';
  }

  getFormPassword()
  {
    return '.PasswordField-password';
  }
  getSignInBtn()
  {
    return '.SignInForm-signInButton';
  }
  getAccountOption()
  {
    return '.CustomerPopup-loggedIn a[href="/customer"]';
  }

  getSizeDropdown()
  {
    return '.Select-control div[id$="--value"]';
  }
  
  getDropdownOptions()
  {
    return '.Select-menu-outer div';
  }

  getAddToBagButton()
  {
    return '.AddToBag';
  }

  getCartItemName()
  {
    return '.MiniCart-items .CartItem-name';
  }

  getCartItemPrice()
  {
    return '.MiniCart-items .CartItem-currentPrice';
  }
  getFacebookLoginButton()
  {
    return '.SignInThirdpartyButtons-facebookButton';
  }
  getClothingButton()
  {
    return '.CategoryNavigation a[href="/women/clothing"]';
  }
  getSearchField()
  {
    return '.QuickSearch-v2-textBox';
  }
  getSearchResults()
  {
    return '.QuickSearch-v2-recentSearchItem';
  }
}
export default HomePage
