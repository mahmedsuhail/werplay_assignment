import 'cypress-file-upload';
import homePage from '../support/page_objects/HomePagePO'

const HomePage = new homePage();



Cypress.Commands.add('clickOnItem', (locator, option, price) => {
  
  cy.get(locator).find('.Product-details').each(($el, index, $list) => {
    if($el.find('div:contains("'+ option + '")').length > 0)
    {
      if($el.find('.Product-minPrice:contains("'+ price + '")').length > 0)
      {
        cy.log(index)
        index=index+1;
        cy.get('.PLP-productList--3 div:nth-child('+ index +') .Product-details span:contains("'+ price + '")').eq(0).click()

      }
    }
  })
})

Cypress.Commands.add('clickCheckboxFromFilters', (option) => {

  cy.get('.GeneralFacet-options .designer a:contains("'+ option + '")').should('be.visible')
  cy.get('.GeneralFacet-options').find('.designer').each(($el, index, $list) => {
    if($el.find('a:contains("'+ option + '")').length > 0)
    {
      cy.get('.GeneralFacet-options .designer input[type="checkbox"]').eq(index).click()
    }
  })
})

Cypress.Commands.add('verifyBrandName', (option) => {

  cy.get('.PLP-productList--3').find('.Product-brand').each(($el, index, $list) => {
    cy.wrap($el).contains(option)
  })
})

Cypress.Commands.add('closeNotificationIfExist', () => {
  cy.wait(5000)
  cy.get("body").then($body => {
    if ($body.find(HomePage.getNoThanksBtn()).length > 0) {   
      cy.get(HomePage.getNoThanksBtn()).click()
      cy.get(HomePage.getNoThanksBtn()).should('not.exist')
    }
});
})